//
//  ViewController.swift
//  Palindrome
//
//  Created by Lipatov on 7/28/17.
//  Copyright © 2017 Lipatov. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var palindromeTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        palindromeTextField.addTarget(nil, action:Selector(("firstResponderAction:")), for:.editingDidEndOnExit)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func checkValue(_ sender: Any)
    {
        let palindromeString = self.palindromeTextField.text // input value
        
        if isPalindrome(str: palindromeString!)
        {
        
            let resultString = String(format: "%@", "This is palindrome:", palindromeString!)
            let alert = UIAlertController(title: "Alert", message:resultString, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))

            self.present(alert, animated: true, completion: nil)
        }else
        {
            let resultString = String(format: "%@", "This is not palindrome:", palindromeString!)
            let alert = UIAlertController(title: "Alert", message:resultString, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))

            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func isPalindrome(str: String) -> Bool
    {
        for i in 0..<str.characters.count/2
        {
            let endIndex = str.characters.count-i-1
            if str[str.index(str.startIndex, offsetBy: i)] != str[str.index(str.startIndex, offsetBy: endIndex)]
            {
                return false
            }
        }
        return true
    }
}

